Release Notes for the CSPP-SystemMonitor Project
===========================================
This LabVIEW project _CSPP-SystemMonitor.lvproj_ is used to develop an application publishing some system parameters.

Version 0.0.0.0 09-08-2019 H.Brand@gsi.de
--------------------------------------
The CSPP-SystemMonitor project was just forked. There is the master branch with some submodules, only.

