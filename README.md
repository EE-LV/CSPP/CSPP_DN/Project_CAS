CSPP-SystemMonitor README
====================
This LabVIEW project _CSPP-SystemMonitor.lvproj_ is used to develop an application publishing some system parameters.

Currently used development SW is LabVIEW 2019.

Related documents and information
=================================
- README.md
- Release_Notes.md
- EUPL v.1.1 - Lizenz.pdf & EUPL v.1.1 - Lizenz.rtf
- Contact: your email
- Download, bug reports... : Git Repository URL
- Documentation:
  - Refer to Documantation folder 
  - NI Actor Framework: https://ni.com/actorframework
  - CS++
	- https://git.gsi.de/EE-LV/CSPP/CSPP/wikis/home
    - https://git.gsi.de/EE-LV/CSPP/CSPP_Documentation
  
Included Submodules
===================
- [Packages/CSPP_Core](https://git.gsi.de/EE-LV/CSPP/CSPP_Core): This package is used as submodule.
- [Packages/CSPP_ObjectManager](https://git.gsi.de/EE-LV/CSPP/CSPP_ObjectManager): This package is used as submodule.
- [Packages/CSPP_DSC](https://git.gsi.de/EE-LV/CSPP/CSPP_DSC): Containing DSC Alarm- & Trend-Viewer
- [Packages/CSPP_Utilities](https://git.gsi.de/EE-LV/CSPP/CSPP_Utilities): Providing some usefull utility classes. 
- [Packages/CSPP_DIM](https://git.gsi.de/EE-LV/CSPP/CSPP_DIM): Implementations of DIM actors and classes.
- [Packages/CSPP_DimLVEvent](https://git.gsi.de/EE-LV/Drivers/DimLVEvent.git: Implementations of DIM interface libraries.

Getting started:
=================================
- Install __CSPP_Tools__
  - Clone [CSPP_Tools](https://git.gsi.de/EE-LV/CSPP/CSPP_Tools)
  - Get submodules:
    - `git submodule init`
    - `git submodule update`
    - _Optionally switch to the most recent branch._
  - Mass-compile folder `CSPP_Tools`
  - Run `CSPP_Tools\Main-Project\Installer.vi`
- Fork __this__ repository, if not alread done, and rename repository name and path. Refer to repository settings.
- Clone the forked repository to a local folder.
- Switch to the desired branch.
- Get submodules:
  - `git submodule init`
  - `git submodule update`
  - _Optionally switch to the most recent branch._
- `chmod a-w -R *` to avoid unintended changes.
- Optionally create a hard link to the custom error file(s): 
  - cd <LabVIEW>\user.lib\errors
  - mklink /h CSPP_Core-errors.txt Packages\CSPP_Core\CSPP_Core-errors.txt
- Rename `CSPP-SystemMonitor.lvproj` to `YourProject.lvproj`
- Open `YourProject.lvproj` with LabVIEW
- Rename following files:
  - `CSPP-SystemMonitor.ini` to `YourProject.ini`
  - `CSPP-SystemMonitor.lvlib` to `YourProject.lvlib`
  - `CSPP-SystemMonitor_Main.vi` to `YourProject_Main.vi`
  - `CSPP-SystemMonitor_Content.vi` to `YourProject_Content.vi`
  - `CSPP-SystemMonitor.lvlib` to `YourProject.lvlib`
- You need to create and deploy your project specific shared variable libraries.
  - Sample shared variable libraries should be available on disk in the corresponding package folder.
  - e.g. copy CSPP_SystemManager_SV.lvlib and rename corresponding URLs in ini.file.
- Run your project specific `YourProject_Main.vi` in order to check if everything is working.
- If using DIM:
  - Download and extract DIM stuff from [CERN](https://dim.web.cern.ch/)
  - Set environment variables DIM_DNS_NODE and include dim\bin32 to PATH.
  - run dns.exe if not already executed elsewhere.
- Extend `YourProject.lvproj` to your needs.

Known issues:
=============
- LabVIEW or the executable may need to be launched with adminitrator permissions.

Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2019  GSI Helmholtzzentrum für Schwerionenforschung GmbH

EEL, Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.